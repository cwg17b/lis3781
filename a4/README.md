# lis 3781 Advanced Database Management

## Christopher Gregor

### Assignment 4 Requirements:

*Ms SQL Sever:*

1. Using Remote Labs, log into SQL Server
2. Must populate tables using T-sql *or* Table Designer

#### Business Rules:

* A sales representative has at least one customer, and each customer has at least one sales rep on any given day(as it is a high-volume organization).
* A customer places at least one order. However, each order is placed by only one customer.
* Each order contains at least one order line.Conversely, each order line is contained in exactly one order.
* Each product may be on a number of order lines.Though, each order line containsexactly one productid (though, each product idmay have a quantity of more than one included, e.g., “oln_qty”).
* Each order is billed on one invoice, and each invoice is a bill for exactly one order(by only one customer).
* An invoice can have one (full),or can have many payments (partial). Though, each payment is made to only one invoice.
* A store hasmany invoices, but each invoice is associated with only one store.
* A vendor providesmany products, but each product is provided by only one vendor.
* Must track yearly history of sales reps, including(also, see Entity-specific attributesbelow): yearly sales goal, yearly total sales, yearly total commission (in dollars and cents).
* Must track history of products, including: cost, price, and discount percentage (if any).


#### Deliverables:

1. ERD
2. SQL Statement Questions
 

#### Assignment Screenshots:

*Screenshot of ERD*:

![A4_ERD_Screenshot](img/a4_ERD.PNG)
