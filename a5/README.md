# LIS 3781 Advanced Database Management

## Christopher Gregor

### Assignment 5 Requirements:

*Expand database from A4 to include:*

1. Region
2. State
3. City
4. Store

#### README.md file should include the following items:

* Screenshot of your ERD
* Bitbucket repo link

#### Deliverables:

* ERD (must be made in MS SQL Server)
* SQL Statement Questions


#### Assignment Screenshots:

*Screenshot of your ERD:

![A5 ERD Screenshot](img/A5_ERD.PNG)



#### Tutorial Links:

*Bitbucket Repo link:*
[Bitbucket Repo link](https://bitbucket.org/cwg17b/lis3781/src/master/ "Bitbucket Repo")
