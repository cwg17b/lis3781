# LIS 3781 Advanced Database Management

## Christopher Gregor

### Assignment 2 Requirements:

*Using only SQL: Save as lis3781_a2_solutions.sql*

A. Tables and insert statements.
B. Include indexes and froeign key SQL statements (see below).
C. Include your query result sets, including grant statements.
D. The following tables should be created and populated with at least 5 records both locally and to the CCI server
E. No Credit will be given if tables and data do not foraward-engineer to the CCI server.

1. Using SQL ONLY, NOT MySQL Workbench:
2. Locally: create yourfsuid database, and two tables: company and customer
	Note: Also, these two tables must be populated in yourfsuid database on the CCI Sever.
>
		a. Use 1:M realtionship: compnay is parent table
		b. company attributes:
>			1. cmp_id (pk)
>			2. cmp_type enum('C-Corp','S-corp','Non-Profit-Corp','LLC','Partnership')
>			3. cmp_street
>			4. cmp_city
>			5. cmp_state
>			6. cmp_zip (zf)
>			7. cmp_phone
>			8. cmp_ytd_sales
>			9. cmp_url
>			10.cmp_notes
		c. customer attributes:
>			1. cus_id (pk)
>			2. cmp_id (fk)
>			3. cus_ssn(binary 64)
>			4. cus_salt(binary 64)
>			5. cus_type enum('Loyal','Discount','Impulse','Need-Based','Wandering')
>			6. cus_first
>			7. cus_last
>			8. cus_street
>			9. cus_city
>			10. cus_state
>			11. cus_zip (zf)
>			12. cus_cus_phone
>			13. cus_email
>			14. cus_balance
>			15. cus_tot_sales
>			16. cus_notes
		d. create suitable indexes and foreign keys:
			Enforce pk/fk relationship: on update cascade, ondelete restrict

#### README.md file should include the following items:

* Screenshot of your SQL code;
* Screenshot of your populated tables;

#### Assignment Screenshots:

*Screenshot of SQL code*:

![SQL code Screenshot](img/sql_code.PNG)

*Screenshot of populated tables*:

![Populated tables Screenshot](img/table_pic.PNG)

