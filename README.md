# LIS3781 Advanced Database Management

## Christopher Gregor

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Provide Screenshots of installations
	- Create Bitbucket repo
	- Complete Bitbucket tutorial (bitbucketstationlocations)
	- Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Using only SQL make the tables compnay and customer, and populate them
	- Create User 1 and give them select, update,  and delete priveleges on both tables
	- Create User 2 and give them select and insert privileges on customer table

3. [A3 README.md](a3/README.md "My A3 README.me file")
	- Log into Oracle Server using Remote Labs
	- Create and populate Oracle tables using only sql
	- Complete required items on Oracle Server

4. [A4 README.md](a4/README.md "My A4 README.me file")
	- Using only Ms SQL create and populate database
	- Complete SQL statements creating triggers and procedures
	- Using T-SQL make ERD
	- DO NOT USE MYSQL

5. [A5 README.md](a5/README.md "My A5 README.me file")
	- Using only Ms SQL create and populate a Region, State, City, and Store tables
	- Complete SQL statements creating triggers and procedures
	- Using T-SQL make ERD
	- DO NOT USE MYSQL

6. [P1 README.md](p1/README.md "My P1 README.me file")
	- Create a database following provided business rules
	- Create an ERD of database
	- Complete sql problems in lis3781_p1_solutions.sql

7. [P2 README.md](p2/README.md "My P2 README.me file")
	- Download and Run MongoDB
	- Import data into collection
	- Complete queries 