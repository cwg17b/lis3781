> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 Advanced Database Management

## Christopher Gregor

### Assignment 1 Requirements:

*Five Parts:*

1. Distributed Version Control with Git and Bitbucket
2. AMPPS Installation
3. Questions
4. Entity Relationship Diagram, and SQL Code (optional)
5. Bitbucket repo links
	a) this assignment
	b) the completed tutorial (bitbucketstationlocations)

> # A1 Database Business Rules:
> The human resource (HR) department of the ACME company wants to contract a database modeler/designer to collect the following employee data for tax purposes:job description, length of employment, benefits,number ofdependents and their relationships, DOB of both the employee and any respective dependents. In addition, employees’ histories mustbe tracked. Also, include the following business rules:
> 
> * Each employee may have one or more dependents
> * Each employee has only one job
> * Each job can be held by many employees
> * Many employees may recieve many benefits
> * Many benefits may be selected by many employees (though, while they may not select any benefits- any dependents of employees may be on an employee's plan)
>   
> Notes:
> 
> * Employee/Dependent tables must use suitable attributes (See Assignment Guidelines);
> 
> In Addition:
> 
> * Employee: SSN, DOB, start/end dates, salary;
> * Dependent: same information as their associated employee (though, not start/end dates), date added (as dependent), type of relationship: e.g., father, mother, etc.
> * Job: title (e.g., secretary, service tech., manager, cashier, janitor, IT, etc.)
> * Benefit: name (e.g., medical, dental, long-term disability, 401k, term life insurance, etc.)
> * Plan: type (single, spouse, family), cost, election date (plans must be unique)
> * Employee history: jobs, salaries, and benefit changes, as well as who made the change and why;
> * Zero Filled data: SSN, zip codes (not phone numbers: US area codes not below 201, NJ);
> * *All* tables must include notes attribute

#### README.md file should include the following items:

* Screenshot of ampps installation running
* git commands w/short descriptions
* A1 ERD image
* Bitbucket repo links for:
	- BitBucketStationLocations
	- This assignment

#### Git commands w/short descriptions:

1. git init - creates a new local git repository
2. git status - List the files you've changed and those you still need to add or commit
3. git add - add files for staging
4. git commit - commit changes to head (-m to add message)
5. git push - send changes to the master branch of your remote repository
6. git pull  - fetch and merge changes on the remote server to your working directory
7. git diff - view all the merge conflicts


#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps_screenshot.PNG "ampps screenshot")

*Screenshot of A1 ERD*:

![My A1 ERD](img/A1_ERD.png)

*Screenshot of A1 EX1
![My A1 EX1](img/A1_EX1.PNG)


#### Tutorial Link:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/cwg17b/bitbucketstationlocations/ "Bitbucket Station Locations")

*A1 README:*
[A1 README](a1/README.md "My A1 README.md file")
