-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema cwg17b
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `cwg17b` ;

-- -----------------------------------------------------
-- Schema cwg17b
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `cwg17b` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `cwg17b` ;

-- -----------------------------------------------------
-- Table `cwg17b`.`job`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cwg17b`.`job` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `cwg17b`.`job` (
  `job_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `job_title` VARCHAR(45) NOT NULL,
  `job_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`job_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `cwg17b`.`employee`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cwg17b`.`employee` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `cwg17b`.`employee` (
  `emp_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `job_id` TINYINT UNSIGNED NOT NULL,
  `emp_ssn` INT(9) UNSIGNED ZEROFILL NOT NULL,
  `emp_fname` VARCHAR(15) NOT NULL,
  `emp_lname` VARCHAR(30) NOT NULL,
  `emp_dob` DATE NOT NULL,
  `emp_start_date` DATE NOT NULL,
  `emp_end_date` DATE NULL,
  `emp_salary` DECIMAL(8,2) NOT NULL,
  `emp_street` VARCHAR(30) NOT NULL,
  `emp_city` VARCHAR(20) NOT NULL,
  `emp_state` CHAR(2) NOT NULL,
  `emp_zip` INT(9) UNSIGNED ZEROFILL NOT NULL,
  `emp_phone` BIGINT UNSIGNED NOT NULL,
  `emp_email` VARCHAR(100) NOT NULL,
  `emp_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`emp_id`),
  UNIQUE INDEX `emp_ssn_UNIQUE` (`emp_ssn` ASC) VISIBLE,
  INDEX `fk_employee_job1_idx` (`job_id` ASC) VISIBLE,
  CONSTRAINT `fk_employee_job1`
    FOREIGN KEY (`job_id`)
    REFERENCES `cwg17b`.`job` (`job_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `cwg17b`.`dependent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cwg17b`.`dependent` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `cwg17b`.`dependent` (
  `dep_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `emp_id` SMALLINT UNSIGNED NOT NULL,
  `dep_added` DATE NOT NULL,
  `dep_ssn` INT(9) ZEROFILL UNSIGNED NOT NULL,
  `dep_fname` VARCHAR(15) NOT NULL,
  `dep_lname` VARCHAR(30) NOT NULL,
  `dep_dob` DATE NOT NULL,
  `dep_relation` VARCHAR(20) NOT NULL,
  `dep_street` VARCHAR(30) NOT NULL,
  `dep_city` VARCHAR(20) NOT NULL,
  `dep_state` CHAR(2) NOT NULL,
  `dep_zip` INT(9) ZEROFILL UNSIGNED NOT NULL,
  `dep_phone` BIGINT NOT NULL,
  `dep_email` VARCHAR(100) NULL,
  `dep_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`dep_id`),
  UNIQUE INDEX `dep_ssn_UNIQUE` (`dep_ssn` ASC) VISIBLE,
  INDEX `fk_dependent_employee_idx` (`emp_id` ASC) VISIBLE,
  CONSTRAINT `fk_dependent_employee`
    FOREIGN KEY (`emp_id`)
    REFERENCES `cwg17b`.`employee` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `cwg17b`.`emp_hist`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cwg17b`.`emp_hist` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `cwg17b`.`emp_hist` (
  `eht_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `emp_id` SMALLINT UNSIGNED NULL,
  `eht_date` DATETIME NOT NULL DEFAULT current_timestamp,
  `eht_type` ENUM('i', 'u', 'd') NOT NULL DEFAULT 'i',
  `eht_job_id` TINYINT UNSIGNED NOT NULL,
  `eht_emp_salary` DECIMAL(8,2) NOT NULL,
  `eht_usr_changed` VARCHAR(30) NOT NULL,
  `eht_reason` VARCHAR(45) NOT NULL,
  `eht_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`eht_id`),
  INDEX `fk_emp_hist_employee1_idx` (`emp_id` ASC) VISIBLE,
  CONSTRAINT `fk_emp_hist_employee1`
    FOREIGN KEY (`emp_id`)
    REFERENCES `cwg17b`.`employee` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `cwg17b`.`benefit`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cwg17b`.`benefit` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `cwg17b`.`benefit` (
  `ben_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `ben_name` VARCHAR(45) NOT NULL,
  `ben_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`ben_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `cwg17b`.`plan`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cwg17b`.`plan` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `cwg17b`.`plan` (
  `pln_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `emp_id` SMALLINT UNSIGNED NOT NULL,
  `ben_id` TINYINT UNSIGNED NOT NULL,
  `pln_type` ENUM('single', 'spouse', 'family') NOT NULL,
  `pln_cost` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pln_election_date` DATE NOT NULL,
  `pln_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pln_id`),
  INDEX `fk_plan_employee1_idx` (`emp_id` ASC) VISIBLE,
  INDEX `fk_plan_benefit1_idx` (`ben_id` ASC) VISIBLE,
  CONSTRAINT `fk_plan_employee1`
    FOREIGN KEY (`emp_id`)
    REFERENCES `cwg17b`.`employee` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_plan_benefit1`
    FOREIGN KEY (`ben_id`)
    REFERENCES `cwg17b`.`benefit` (`ben_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `cwg17b`.`job`
-- -----------------------------------------------------
START TRANSACTION;
USE `cwg17b`;
INSERT INTO `cwg17b`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'doctor', NULL);
INSERT INTO `cwg17b`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'magician', NULL);
INSERT INTO `cwg17b`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'demon cultist', NULL);
INSERT INTO `cwg17b`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'demon remover', NULL);
INSERT INTO `cwg17b`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'pizza delivery driver', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `cwg17b`.`employee`
-- -----------------------------------------------------
START TRANSACTION;
USE `cwg17b`;
INSERT INTO `cwg17b`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (DEFAULT, 1, 567892844, 'John', 'Smith', '1937-10-11', '2000-01-01', NULL, 464732.86, '602 Lamp ln', 'Tallahassee', 'fl', 747264746, 7649382546, 'johnsmith@email.com', NULL);
INSERT INTO `cwg17b`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (DEFAULT, 2, 475728765, 'Vallarie', 'Cain', '1984-01-07', '2000-02-01', NULL, 547563.47, '746 Bog Blvd', 'St. Augustine', 'fl', 732646235, 4637285455, 'froglord@email.com', NULL);
INSERT INTO `cwg17b`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (DEFAULT, 3, 638264532, 'Clair', 'Silver', '1934-09-09', '2001-03-08', NULL, 253476.90, '222 Cheese Cloth ave', 'Solon', 'oh', 978236453, 9083642633, 'thegreatcheese13@aol.com', NULL);
INSERT INTO `cwg17b`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (DEFAULT, 4, 245465878, 'Rak', 'Rotom', '1976-07-07', '2010-11-03', '2019-10-30', 567928.34, '9276 Pork Qt', 'Seattle', 'wa', 927234563, 8265463843, 'doomday4463@email.com', NULL);
INSERT INTO `cwg17b`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (DEFAULT, 5, 032949723, 'Valyra', 'Bean', '1865-01-01', '2002-03-18', NULL, 625743.29, '35 Universe Rd', 'Austin', 'tx', 124536474, 2435678444, 'cardcardman33@email.com', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `cwg17b`.`dependent`
-- -----------------------------------------------------
START TRANSACTION;
USE `cwg17b`;
INSERT INTO `cwg17b`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (DEFAULT, 1, '2010-01-12', 123847320, 'Jack', 'Smith', '1987-11-18', 'father', '673 Fall ln', 'Solon', 'oh', 636872974, 7594672746, NULL, NULL);
INSERT INTO `cwg17b`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (DEFAULT, 2, '2011-12-19', 527283731, 'Molly', 'Lucien', '1976-08-15', 'mother', '273 Cookie Dr', 'Frog land', 'fl', 832746535, 9873246653, 'frogmanmanny@email.com', NULL);
INSERT INTO `cwg17b`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (DEFAULT, 3, '2018-11-18', 748235462, 'Fjord', 'Ship', '1954-01-01', 'brother', '209 Lork Ln', 'Orlando', 'fl', 245632888, 9863245627, 'thekingcheese@email.com', NULL);
INSERT INTO `cwg17b`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (DEFAULT, 4, '2000-03-10', 823843264, 'Jester', 'Lavore', '1936-02-02', 'captain', '534 Water Pier Dr', 'Tampa', 'fl', 992783645, 5638476473, NULL, NULL);
INSERT INTO `cwg17b`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (DEFAULT, 5, '2001-08-12', 902974003, 'Caleb', 'Widogast', '1983-04-15', 'spouse', '642 Trost Rd', 'Tallahassee', 'fl', 264563822, 2376456383, NULL, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `cwg17b`.`emp_hist`
-- -----------------------------------------------------
START TRANSACTION;
USE `cwg17b`;
INSERT INTO `cwg17b`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_usr_changed`, `eht_reason`, `eht_notes`) VALUES (DEFAULT, 1, '2011-07-15', 'u', 13, 647238.63, 'test', 'cost', NULL);
INSERT INTO `cwg17b`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_usr_changed`, `eht_reason`, `eht_notes`) VALUES (DEFAULT, 2, '2016-01-18', 'i', 01, 127348.64, 'test', 'new provider', NULL);
INSERT INTO `cwg17b`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_usr_changed`, `eht_reason`, `eht_notes`) VALUES (DEFAULT, 3, '2000-11-11', 'd', 82, 347823.65, 'test', 'false information', NULL);
INSERT INTO `cwg17b`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_usr_changed`, `eht_reason`, `eht_notes`) VALUES (DEFAULT, 4, '1788-09-18', 'u', 26, 653489.24, 'test', 'identity theft', NULL);
INSERT INTO `cwg17b`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_usr_changed`, `eht_reason`, `eht_notes`) VALUES (DEFAULT, 5, '2020-10-20', 'i', 18, 523643.82, 'test', 'new job', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `cwg17b`.`benefit`
-- -----------------------------------------------------
START TRANSACTION;
USE `cwg17b`;
INSERT INTO `cwg17b`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (DEFAULT, 'medical', NULL);
INSERT INTO `cwg17b`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (DEFAULT, 'dental', NULL);
INSERT INTO `cwg17b`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (DEFAULT, '401k', NULL);
INSERT INTO `cwg17b`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (DEFAULT, 'long-term disability', NULL);
INSERT INTO `cwg17b`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (DEFAULT, 'term life insurance', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `cwg17b`.`plan`
-- -----------------------------------------------------
START TRANSACTION;
USE `cwg17b`;
INSERT INTO `cwg17b`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (DEFAULT, 1, 1, 'single', 6473.76, '2010-01-01', NULL);
INSERT INTO `cwg17b`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (DEFAULT, 2, 2, 'spouse', 8974.23, '2009-04-21', NULL);
INSERT INTO `cwg17b`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (DEFAULT, 3, 3, 'family', 6482.63, '2019-12-17', NULL);
INSERT INTO `cwg17b`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (DEFAULT, 4, 4, 'single', 9297.34, '2000-03-07', NULL);
INSERT INTO `cwg17b`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (DEFAULT, 5, 5, 'spouse', 1332.44, '2019-09-01', NULL);

COMMIT;

