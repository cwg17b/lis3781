# lis 3781 - Advanced Database Management

## Christopher Gregor

### Project 1 Requirements:

*Deliverables:*

1. ERD in .mwb format
2. SQL statement questions


#### README.md file should include the following items:

* Screenshot of your ERD;
* Optional: SQL code for the required reports
* Bitbucket repo links: Your lis3781 Bitbucket repo link
 

#### Business Rules

* An attorney is retainedby (or assigned to)one or moreclients,for each case.
* A client has(or is assigned to) one or more attorneysfor each case.
* An attorney hasone or more cases.
* A client has one or more cases.
* Each court has one or more judges adjudicating.
* Each judge adjudicates upon exactly one court.
* Each judgemaypresideovermore than one case.
* Each case that goes to court ispresided over by exactly one judge.
* A person can have more than one phone number.

#### Assignment Screenshots:

*Screenshot of ERD*:

![ERD Screenshot](img/p1_ERD.png)


